/**
 * FlexSolution root namespace.
 *
 * @namespace FlexSolution
 */
if (typeof FlexSolution === undefined || !FlexSolution) {
    var FlexSolution = {};
}

/**
 * FlexSolution dashlet namespace.
 *
 * @namespace FlexSolution.dashlet
 */
if (typeof FlexSolution.dashlet === undefined || !FlexSolution.dashlet) {
    FlexSolution.dashlet = {};
}

/**
 * Dashboard FileSearch component.
 *
 * @namespace FlexSolution.dashlet
 * @class FlexSolution.dashlet.FileSearch
 */
(function () {

    var Dom = YAHOO.util.Dom;

    var $html = Alfresco.util.encodeHTML;

    FlexSolution.dashlet.FileSearch = function FileSearch_constructor(htmlId) {
        return FlexSolution.dashlet.FileSearch.superclass.constructor.call(this, "FlexSolution.dashlet.FileSearch", htmlId);
    };

    YAHOO.extend(FlexSolution.dashlet.FileSearch, Alfresco.component.Base, {

        options: {},

        onReady: function FileSearch_onReady() {
            this.widgets.submitButton = Alfresco.util.createYUIButton(this, "submitButton", this.onButtonClick);
        },

        onButtonClick: function FileSearch_onButtonClick(e) {
            Alfresco.util.Ajax.jsonGet({
                url: this.buildRequestUrl(),
                successCallback: {
                    fn: this.onSuccess,
                    scope: this
                },
                failureCallback: {
                    fn: this.onFailure,
                    scope: this
                }
            });
        },

        buildRequestUrl: function FileSearch_buildRequestUrl() {
            var url = Alfresco.constants.PROXY_URI + "search/search-file-by-name?fileName={fileName}&site={site}";
            return YAHOO.lang.substitute(url, {
                fileName: Dom.get(this.id + "-searchText").value,
                site: Alfresco.constants.SITE
            });
        },

        onSuccess: function FileSearch_onSuccess(resp) {
            var cont = Dom.get(this.id + "-result");
            cont.innerHTML = "";
            var array = resp.json.files;
            if(array.length === 0) {
                var li = document.createElement("li");
                var text = document.createTextNode("No files found for specified request.");
                li.appendChild(text);
                cont.appendChild(li);
            }
            for(var i = 0; i < array.length; i++) {
                var li = document.createElement("li");
                var a = document.createElement("a");
                var link = "/share/page";
                var siteShortName = Alfresco.constants.SITE;
                if(siteShortName !== null && siteShortName.length > 0) {
                    link += "/site/" + siteShortName;
                }
                link += "/document-details?nodeRef=" + array[i].id;
                a.setAttribute("href", link);
                var text = document.createTextNode(array[i].name);
                a.appendChild(text);
                li.appendChild(a);
                cont.appendChild(li);
            }
        },

        onFailure: function FileSearch_onFailure(resp) {
            Alfresco.util.PopupManager.displayPrompt({
                text: resp.serverResponse.status + " - " + resp.serverResponse.statusText
            });
        }
    });

})();