define(["dojo/_base/declare",
        "dijit/_WidgetBase",
        "alfresco/core/Core",
        "dijit/_TemplatedMixin",
        "dojo/text!./templates/HelloWidget.html"
    ],
    function (declare, _Widget, Core, _Templated, template) {
        return declare([_Widget, Core, _Templated], {
            templateString: template,
            i18nRequirements: [ {i18nFile: "./i18n/HelloWidget.properties"} ],
            cssRequirements: [{cssFile:"./css/HelloWidget.css"}],

            buildRendering: function example_widgets_HelloWidget__buildRendering() {
                this.helloMsg = this.message('msg.hello');
                this.inherited(arguments);

            }
        });
    }
);