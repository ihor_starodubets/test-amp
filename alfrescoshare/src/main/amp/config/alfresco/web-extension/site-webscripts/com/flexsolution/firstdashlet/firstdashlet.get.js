function main() {

    var fileSearch = {
        id: "FileSearch",
        name: "FlexSolution.dashlet.FileSearch"
    };

    var dashletResizer = {
        id: "DashletResizer",
        name: "Alfresco.widget.DashletResizer",
        initArgs: ["\"" + args.htmlid + "\"", "\"" + instance.object.id + "\""],
        useMessages: false
    };

    model.widgets = [fileSearch, dashletResizer];
}

main();

