<@markup id="css" >
    <@link rel="stylesheet" type="text/css" href="${url.context}/res/components/dashlets/file-search.css" group="dashlets"  />
</@>

<@markup id="js">
   <@script type="text/javascript" src="${url.context}/res/components/dashlets/file-search.js" group="dashlets"/>
</@>

<@markup id="widgets">
    <@createWidgets group="dashlets"/>
</@>

<@markup id="html">
    <@uniqueIdDiv>
        <#assign id = args.htmlid?html>
        <div class="dashlet fileSearchDashlet">
            <div class="title">${msg("searchFile.title")}</div>
            <div class="toolbar">
                <div class="fileSearchInput">
                    <input type="text" id="${id}-searchText">
                </div>
                <button id="${id}-submitButton">${msg("searchFile.searchButton")}</button>
            </div>
            <div class="body fileSearchResult scrollableList" <#if args.height??>style="height: ${args.height?html}px;"</#if>>
                <ul id="${id}-result"></ul>
            </div>
        </div>
    </@>
</@>