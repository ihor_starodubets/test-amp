//Move file actions example
var move = actions.create("email");
move.parameters["destination-folder"] = search.findNode("workspace://SpacesStore/f48c4914-bd1e-4fdb-b63a-89825044a11a");
move.parameters.keywords = "alfresco, webscript, node";
move.execute(search.findNode("workspace://SpacesStore/a15780d0-50be-4935-ad4e-a42d5e8d691e"));


//Send mail actions example
var mail = actions.create("mail");
mail.parameters.subject = "Notification";
mail.parameters.from = "i.starodubets@flex-solution.com";
mail.parameters.to = "i.starodubets@flex-solution.com";
mail.parameters.text = "Bla-bla-bla";
mail.execute(companyhome);