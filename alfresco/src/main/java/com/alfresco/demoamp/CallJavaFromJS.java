package com.alfresco.demoamp;

import org.alfresco.repo.processor.BaseProcessorExtension;

public class CallJavaFromJS extends BaseProcessorExtension {

    public String greeting(String name) {
        return "Hello " + name;
    }

    public double getHypotenuseLength(int cathetus1, int cathetus2) {
        return Math.sqrt(Math.pow(cathetus1, 2) + Math.pow(cathetus2, 2));
    }
}
