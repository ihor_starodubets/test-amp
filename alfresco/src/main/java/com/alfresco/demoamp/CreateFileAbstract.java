package com.alfresco.demoamp;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;
import java.io.Writer;

public class CreateFileAbstract extends AbstractWebScript {

    private NodeService nodeService;
    private FileFolderService fileFolderService;

    @Override
    public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
        String folder = req.getParameter("folderName");
        String file = req.getParameter("fileName");

        if(StringUtils.isBlank(folder)) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "Folder cannot be empty");
        }
        NodeRef folderNodeRef = new NodeRef(folder);

        if(!nodeService.exists(folderNodeRef)) {
            throw new WebScriptException(Status.STATUS_NOT_FOUND, "Folder with nodref " + folder + " not found");
        } else if(StringUtils.isBlank(file)) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "File name '" + file + "' is not correct");
        }
        else {
            try(Writer writer = res.getWriter()) {
                fileFolderService.create(new NodeRef(folder), file, ContentModel.TYPE_CONTENT);
                writer.write("File with name '" + file + "' successfully created.");
            } catch (FileExistsException e) {
                res.setStatus(Status.STATUS_INTERNAL_SERVER_ERROR);
            }
        }
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }
}
