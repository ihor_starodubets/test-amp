package com.alfresco.demoamp;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.springframework.extensions.webscripts.*;

import java.util.HashMap;
import java.util.Map;

public class CreateFile extends DeclarativeWebScript {

    public static final String FILE_NAME = "fileName";
    private NodeService nodeService;
    private FileFolderService fileFolderService;

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest request, Status status, Cache cache) {

        String folder = request.getParameter("folderName");
        String file = request.getParameter("fileName");

        if(StringUtils.isBlank(folder)) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "Folder cannot be empty");
        }

        NodeRef folderNodeRef = new NodeRef(folder);

        if(!nodeService.exists(folderNodeRef)) {
            throw new WebScriptException(Status.STATUS_NOT_FOUND, "Folder with nodref " + folder + " not found");
        } else if(StringUtils.isBlank(file)) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "File name '" + file + "' is not correct");
        }
        else {
            try {
                fileFolderService.create(new NodeRef(folder), file, ContentModel.TYPE_CONTENT);
            } catch(FileExistsException e) {
                status.setCode(Status.STATUS_INTERNAL_SERVER_ERROR,
                        "File with name '" + file + "' already exist");
                status.setRedirect(true);
            }
            /*nodeService.createNode(folderNodeRef, ContentModel.ASSOC_CONTAINS,
                    QName.createQName("something"), ContentModel.TYPE_CONTENT);*/
        }
        Map<String, Object> model = new HashMap<>();
        model.put(FILE_NAME, file);
        return model;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }
}
