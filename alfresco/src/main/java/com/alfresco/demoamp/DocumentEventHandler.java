package com.alfresco.demoamp;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.MailActionExecuter;
import org.alfresco.repo.admin.SysAdminParams;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.site.SiteInfo;
import org.alfresco.service.cmr.site.SiteService;
import org.apache.log4j.Logger;

/**
 * Created by ihor on 1/10/17.
 */
public class DocumentEventHandler implements NodeServicePolicies.OnCreateNodePolicy {

    private static final Logger LOGGER = Logger.getLogger(DocumentEventHandler.class);

    private NodeService nodeService;
    private PolicyComponent policyComponent;
    private ActionService actionService;
    private SysAdminParams sysAdminParams;
    private SiteService siteService;

    public void registerEventHandler() {

        this.policyComponent.bindClassBehaviour(NodeServicePolicies.OnCreateNodePolicy.QNAME,
                ContentModel.TYPE_CONTENT, new JavaBehaviour(this, "onCreateNode"));
    }

    @Override
    public void onCreateNode(ChildAssociationRef childAssocRef) {

        NodeRef childRef = childAssocRef.getChildRef();

        if(childRef == null) {
            LOGGER.error("NodRef " + childRef + " does not exist");
        }

        String name = (String) nodeService.getProperty(childRef, ContentModel.PROP_NAME);

        if ("admin".contains(name)) {

            SiteInfo site = siteService.getSite(childRef);

            String link = sysAdminParams.getShareProtocol() + "://" +
                    sysAdminParams.getShareHost() + ":" +
                    sysAdminParams.getSharePort() + "/" +
                    sysAdminParams.getShareContext() + "/page";

            if (site != null) {
                link += "/site/" + site.getShortName();
            }

            link += "/document-details?nodeRef=" + childRef;


            Action mailAction = actionService.createAction(MailActionExecuter.NAME);
            mailAction.setParameterValue(MailActionExecuter.PARAM_SUBJECT, "Notification");
            mailAction.setParameterValue(MailActionExecuter.PARAM_TO, "i.starodubets@flex-solution.com");
            mailAction.setParameterValue(MailActionExecuter.PARAM_FROM, "i.starodubets@flex-solution.com");
            mailAction.setParameterValue(MailActionExecuter.PARAM_TEXT, link);
            actionService.executeAction(mailAction, null);
        }
    }

    public void setPolicyComponent(PolicyComponent policyComponent) {
        this.policyComponent = policyComponent;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

    public void setSysAdminParams(SysAdminParams sysAdminParams) {
        this.sysAdminParams = sysAdminParams;
    }

    public void setSiteService(SiteService siteService) {
        this.siteService = siteService;
    }
}
