package com.alfresco.demoamp;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.ResultSetRow;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.extensions.webscripts.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AlfrescoSearch extends DeclarativeWebScript {

    private SearchService searchService;

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest request, Status status, Cache cache) {

        Map<String, Object> model = new HashMap<>();

        String query = request.getParameter("query");

        if(StringUtils.isBlank(query)) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "Query cannot be empty.");
        }

        SearchParameters parameters = new SearchParameters();
        parameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        parameters.setLanguage(searchService.LANGUAGE_LUCENE);
        parameters.setQuery(query);

        ResultSet resultSet = searchService.query(parameters);
        List<NodeRef> nodeRefList = new ArrayList<>();
        for(ResultSetRow row : resultSet) {
            NodeRef nodeRef = row.getNodeRef();
            nodeRefList.add(nodeRef);
        }

        model.put("nodeRefList", nodeRefList);

        return model;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }
}
