package com.alfresco.demoamp;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.*;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by ihor on 1/12/17.
 */
public class MoveFileActionExecutor extends ActionExecuterAbstractBase {

    private static final Logger LOGGER = Logger.getLogger(MoveFileActionExecutor.class);

    private static final String PARAM_DESTINATION_FOLDER = "destination-folder";

    private static final String PARAM_KEYWORDS = "keywords";

    private NodeService nodeService;
    private FileFolderService fileFolderService;
    private ContentService contentService;

    @Override
    protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {

        ContentData contentData = (ContentData) nodeService.getProperty(actionedUponNodeRef, ContentModel.PROP_CONTENT);
        String nodeMimeType = contentData.getMimetype();

        if(!MimetypeMap.MIMETYPE_TEXT_PLAIN.equals(nodeMimeType)) {
            LOGGER.debug("MimeType of NodRef " + actionedUponNodeRef + " is not 'text/plain'.");
            return;
        }

        NodeRef destinationFolder = (NodeRef) action.getParameterValue(PARAM_DESTINATION_FOLDER);

        if(destinationFolder == null) {
            LOGGER.debug("Destination folder is not specified.");
            return;
        }

        String keywords = (String) action.getParameterValue(PARAM_KEYWORDS);

        if(StringUtils.isEmpty(keywords)) {
            LOGGER.debug("Keywords are not specified.");
            return;
        }

        String[] words = keywords.trim().split("\\s*,\\s*");

        ContentReader contentReader = contentService.getReader(actionedUponNodeRef, ContentModel.PROP_CONTENT);

        String content  = contentReader.getContentString();

        if(StringUtils.isEmpty(content)) {
            LOGGER.debug("File is empty.");
            return;
        }

        boolean isContainsKeyword = false;

        for(String word : words) {
            if(content.contains(word)) {
                isContainsKeyword = true;
                break;
            }
        }

        if(isContainsKeyword) {
            try {
                fileFolderService.move(actionedUponNodeRef, destinationFolder, null);
            } catch (FileNotFoundException e) {
                LOGGER.error(e.getMessage());
            }
        }

    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
        paramList.add(
                new ParameterDefinitionImpl(PARAM_DESTINATION_FOLDER, DataTypeDefinition.NODE_REF,
                        true, getParamDisplayLabel(PARAM_DESTINATION_FOLDER)));

        paramList.add(new ParameterDefinitionImpl(PARAM_KEYWORDS, DataTypeDefinition.TEXT,
                true, getParamDisplayLabel(PARAM_KEYWORDS)));
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }
}
