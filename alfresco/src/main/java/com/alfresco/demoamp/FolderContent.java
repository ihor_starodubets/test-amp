package com.alfresco.demoamp;

import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.extensions.webscripts.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FolderContent extends DeclarativeWebScript {

    private NodeService nodeService;
    private FileFolderService fileFolderService;

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest request, Status status, Cache cache) {
        Map<String, Object> model = new HashMap<>();
        String parentFolder = request.getParameter("parentFolder");

        if(StringUtils.isBlank(parentFolder)) {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, "parentFolder cannot be empty.");
        }

        NodeRef parentFolderNodeRef = new NodeRef(parentFolder);

        if(!nodeService.exists(parentFolderNodeRef)) {
            throw new WebScriptException(Status.STATUS_NOT_FOUND,
                    "Folder with nodref '" + parentFolder + "' not found.");
        } else {
            List<FileInfo> listFiles = fileFolderService.listFiles(parentFolderNodeRef);
            List<FileInfo> listFolders = fileFolderService.listFolders(parentFolderNodeRef);
            model.put("listFiles", listFiles);
            model.put("listFolders", listFolders);
        }

        return model;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }
}
