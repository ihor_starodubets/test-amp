var parentFolder = args.parentFolder;
var folderName = args.folderName;
var fileName = args.fileName;

if(parentFolder === null || parentFolder.length === 0) {
    status.code = 400;
    status.message = "Parent folder cannot be empty";
    status.redirect = true;
} else if(folderName === null || folderName.length === 0) {
    status.code = 400;
    status.message = "Folder name cannot be empty"
    status.redirect = true;
} else if(fileName === null || fileName.length === 0) {
    status.code = 400;
    status.message = "File name cannot be empty"
    status.redirect = true;
} else {
    var parentFolderNode = search.findNode(parentFolder);
    if (!parentFolderNode.exists()) {
        status.code = 404;
        status.message = "Folder with nodref '" + parentFolder + "' not found"
        status.redirect = true;
    } else {
        var newFolder = parentFolderNode.createFolder(folderName);

        var newFile = newFolder.createFile(fileName);

        var fileContent = "This is file content";
        newFile.setContent(fileContent);
        newFile.save();
    }
}

model.fileContent = fileContent;
