{
    "files": [
        <#list listFiles as file>
                {
                    "name": "${file.name}",
                    "created": "${file.getCreatedDate()?datetime}",
                    "type": "${file.type.localName}"
                }<#if file_has_next>,</#if>
        </#list>
    ],
    "folders": [
        <#list listFolders as folder>
                {
                    "name": "${folder.name}",
                    "created": "${folder.createdDate?datetime}",
                    "type": "${folder.type.localName}"
                }<#if folder_has_next>,</#if>
        </#list>
    ]
}

