{
        "files": [
            <#list nodes as node>
                {
                    "id": "${node.nodeRef}",
                    "name": "${node.name}"
                }<#if node_has_next>,</#if>
            </#list>
        ]
}
