var file  = args.file;
var folder = args.folder;

if(file === null || file.length === 0) {
    status.code = 400;
    status.message = "File cannot be empty";
    status.redirect = true;
} else if(folder === null || folder.length === 0) {
    status.code = 400;
    status.message = "Folder cannot be empty";
    status.redirect = true;
} else {
    var folderNode = search.findNode(folder);
    var fileNode = search.findNode(file);
    if(!folderNode.exists()) {
        status.code = 404;
        status.message = "Folder with noderef '" + folder + "' not found";
        status.redirect = true;
    } else if(!fileNode.exists()) {
        status.code = 404;
        status.message = "File with noderef '" + file + "' not found";
        status.redirect = true;
    } else {
        fileNode.move(folderNode);
    }
}






