{
    "nodeRef": [
        <#list nodeRefList as nodeRef>
            {
                "name": "${nodeRef.name}",
                "type": "${nodeRef.type.localName}",
                "size": "${nodeRef.size}",
                "path": "${nodeRef.displayPath}"
            }<#if nodeRef_has_next>,</#if>
        </#list>
    ]
}