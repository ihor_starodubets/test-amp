var query = args.query;

if(query == null || query.length === 0) {
    status.code = 400;
    status.message = "Query cannot be empty.";
    status.redirect = true;
}

model.nodes = search.luceneSearch(query);