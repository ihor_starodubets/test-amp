{
    "nodeRef": [
        <#list nodes as node>
            {
                "name": "${node.name}",
                "type": "${node.type.localName}",
                "size": "${node.size}",
                "path": "${node.displayPath}"
            }<#if node_has_next>,</#if>
        </#list>
    ]
}