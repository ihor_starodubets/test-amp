var file = args.file;
var name = args.fileName;

if(file === null || file.length === 0) {
    status.code = 400;
    status.message = "File cannot be empty";
    status.redirect = true;
} else if(name === null || name.length === 0) {
    status.code = 400;
    status.message = "Name cannot be empty";
    status.redirect = true;
} else {
    var fileNode = search.findNode(file);
    if(!fileNode.exists())  {
        status.code = 404;
        status.message = "File with nodref '" + file + "' not found";
        status.redirect = true;
    } else {
        fileNode.setName(name);
        fileNode.save();
    }
}



